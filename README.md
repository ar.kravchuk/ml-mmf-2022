# Репозиторий курса "Машинное обучение"

### Полезные ссылки

* [telegram-чатик курса](https://t.me/ml_mmf_2022)

### Преподаватели

Кравчук Артем Витальевич, @akravchuk97 (tg), a.kravchuk@g.nsu.ru

Кальмуцкий Кирилл Олегович, @Kirill_Kalmutskiy (tg), k.kalmutskii@g.nsu.ru

### Карта курса

* [week-01](week-01): организационные вопросы, numpy, pandas, matplotlib;
* [week-02](week-02): введение в мл, линейная регрессия, логистическая регрессия;
* [week-03](week-03): деревья решений и случайный лес;
* [week-04](week-04): метрические алгоритмы и задача кластеризации;
* [week-05](week-05): метрики
* [week-06](week-06): наивный байесовский классификатор, дисбаланс
* [week-07](week-07): градиентный бустинг
* [week-08](week-08): временные ряды
* [week-09](week-09): EM-алгоритм
* [week-10](week-10): методы понижения размерности и рекомендательные системы
* [week-11](week-11): введение в глубокое обучение
* [week-12](week-12): введение в компьютерное зрение
* [week-13](week-13): введение в обработку естественного языка

### Схема сдачи домашних заданий

1) Создайте **приватный** репозиторий с названием `{surname}-{name}-ml-mmf-2022`;
2) Добавьте `ar.kravchuk` и `KirillKalmutsky` роль `Maintainer` в своем проекте;
3) Склонируйте этот репозиторий себе локально на компьютер;
4) Создайте ветку с названием `{surname}-hw-*`, где * - номер домашнего задания;
5) В этой ветке выложите решение в папку `hw-*`;
6) Создайте `merge request` из cозданной ветки в ветку `master`, в поле `Assignee` добавьте `ar.kravchuk`;
7) Оставьте ссылку на ваш MR в этой [табличке](https://docs.google.com/spreadsheets/d/1H0iG-zhxGle3M_Vz5QpRtvVxNBKK7RsTS4bQ11OQRsc/edit?usp=sharing).

Если домашнее задание представляет собой `jupyter notebook`, то пишите решение прямо в нем в специальных ячейках под условиями задачи. Не удаляйте ячейки с условиями задач и перед сдачей обязательно убедитесь, что ноутбук запускается "сверху вниз" без ошибок.

### Дедлайны

* [hw-01](hw-01): Дедлайн 2 октября, 23:50
* [hw-02](hw-02): Дедлайн 4 декабря, 23:50
* [hw-03](hw-03): Дедлайн 25 декабря, 23:50
